/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot;

import com.ddoggy99.discordbot.config.Config;
import com.ddoggy99.discordbot.music.station.IStation;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Brock on 11/07/2016.
 */
public class StatsServer implements Runnable {

    private ServerSocket socket;

    public StatsServer() {
        try {
            socket = new ServerSocket(5555);
            System.out.println("Running StatsServer on port 5555");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        boolean running;
        running = !socket.isClosed();
        while (running) {
            Socket client = null;
            InputStreamReader in = null;
            BufferedReader reader = null;
            JSONObject json = null;
            try {
                client = socket.accept();
                in = new InputStreamReader(client.getInputStream());
                reader = new BufferedReader(in);
                json = new JSONObject(reader.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (json == null){
                this.handleError(client, 400, "Invalid Request");
                continue;
            }
            if (!this.handleKey(client, json, "method"))
                continue;
            if (!this.handleKey(client, json, "uri"))
                continue;
            String HTTP_METHOD = json.getString("method");
            String URI = json.getString("uri");
            if (HTTP_METHOD.equalsIgnoreCase("GET")){
                String[] split = URI.split(":");
                String request = split[0];
                JSONObject response = new JSONObject();
                switch (request){
                    case "stations":
                        JSONArray array = new JSONArray();
                        for (IStation station : Config.STATIONS.values()){
                            JSONObject jsonStation = new JSONObject();
                            jsonStation.put("name", station.getName());
                            jsonStation.put("state", station.getState());
                        }
                }
            }
        }
    }

    public boolean handleKey(Socket client, JSONObject json, String key) {
        if (key.equalsIgnoreCase("method")) {
            if (json.has(key))
                return true;
            this.handleError(client, 404, "Missing key 'method'");
        }
        return false;
    }

    public void handleError(Socket client, int code, String message) {
        try {
            new BufferedWriter(new OutputStreamWriter(client.getOutputStream())).write(new JSONObject().put("code", code).put("message", message).toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
