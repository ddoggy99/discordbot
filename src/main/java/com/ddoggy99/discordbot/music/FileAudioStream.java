/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.music;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Brock on 5/06/2016.
 */
public class FileAudioStream extends AudioStream {

    private Process process;

    public FileAudioStream(List<String> ffmpegArgs) {
        try {
            ProcessBuilder builder = new ProcessBuilder();
            builder.command(ffmpegArgs);
            System.out.println("Command: " + builder.command());
            process = builder.start();
            this.in = process.getInputStream();

            Thread ytdlErrGobler = new Thread()
            {
                @Override
                public void run()
                {
                    try
                    {
                        InputStream fromYTDL = null;

                        fromYTDL = process.getErrorStream();
                        if (fromYTDL == null)
                            System.out.println("ffmpeg is null");

                        byte[] buffer = new byte[1024];
                        int amountRead = -1;
                        while (!isInterrupted() && ((amountRead = fromYTDL.read(buffer)) > -1))
                        {
                            //System.out.println("ERR ffmpeg: " + new String(Arrays.copyOf(buffer, amountRead)));
                        }
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
            };
            ytdlErrGobler.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void close() throws IOException {
        if (in != null) {
            in.close();
            in = null;
        }
        if (process != null) {
            process.destroy();
            process = null;
        }
        super.close();
    }
}
