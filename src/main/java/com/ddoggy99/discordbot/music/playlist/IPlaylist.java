/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.music.playlist;

import com.ddoggy99.discordbot.music.AudioSource;

/**
 * A collection of songs
 * <p>
 * Created by Brock on 29/06/2016.
 */
public interface IPlaylist {

    /**
     * Gets the previously played song
     *
     * @return Previously played song
     */
    AudioSource previous();

    /**
     * Gets the current song playing
     *
     * @return Current song being played
     */
    AudioSource current();

    /**
     * Gets the next song to play
     *
     * @return Next song to play
     */
    AudioSource next();

    /**
     * Plays the next song
     *
     * @return Current song being played
     */
    AudioSource playNext();

    /**
     * Plays a requested audioSource
     * @param audioSource Requested audioSource
     * @return Requested audioSource if sucessful
     */
    AudioSource playNext(AudioSource audioSource);

    /**
     * Removes the current song and sets it as previous song
     *
     */
    void stop();

    /**
     * Adds a audioSource to the playlist
     *
     * @param audioSource AudioSource to be added
     */
    void addSong(AudioSource audioSource);

    /**
     * Removes a audioSource from the playlist
     *
     * @param audioSource AudioSource to be removed
     */
    void removeSong(AudioSource audioSource);

    /**
     * Checks if the given audioSource is already in the playlist
     * @param audioSource The audioSource to be checked
     * @return True if the audioSource is in the playlist, false if not
     */
    boolean exists(AudioSource audioSource);

    /**
     * Finds a song by the given name
     *
     * @param name    Name of song
     * @param partial If true then it will partial match the name
     * @return The song requested
     */
    AudioSource getSongFromName(String name, boolean partial);

    /**
     * Gets the next song in the playlist, calculates if its a random one or just next in the list
     * @return Next song to be played
     */
    AudioSource getNext();

    /**
     * Resets the playlist back to default
     */
    void reset();

    /**
     * Number of songs in playlist
     * @return Number of songs in playlist
     */
    int size();

    /**
     * Sets if the song list is shuffled or not
     *
     * @param shuffle True if list is shuffled
     */
    void shuffle(boolean shuffle);

    /**
     * Gets if the song list is shuffled or not
     *
     * @return True if list is shuffled, false if not
     */
    boolean isShuffled();

    void repeat(boolean repeat);

    boolean isRepeating();
}
