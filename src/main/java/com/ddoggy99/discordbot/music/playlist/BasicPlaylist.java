/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.music.playlist;

import com.ddoggy99.discordbot.music.AudioSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Brock on 30/06/2016.
 */
public class BasicPlaylist implements IPlaylist {

    private Logger log = LogManager.getLogger(BasicPlaylist.class);

    private boolean repeat = false;
    private boolean shuffle = false;
    private ArrayList<AudioSource> fullList = new ArrayList<>();
    private ArrayList<AudioSource> upcomingAudioSources = new ArrayList<>();
    private ArrayList<AudioSource> playedAudioSources = new ArrayList<>();

    private AudioSource previousAudioSource = null;
    private AudioSource currentAudioSource = null;
    private AudioSource nextAudioSource = null;

    public AudioSource previous() {
        return this.previousAudioSource;
    }

    public AudioSource current() {
        return this.currentAudioSource;
    }

    public AudioSource next() {
        return this.nextAudioSource;
    }

    public AudioSource playNext(){
        return this.playNext(null);
    }

    public AudioSource playNext(AudioSource request) {
        if (request != null){
            this.nextAudioSource = request;
            log.info("Playing requested song " + request.getSong());
        }
        // If theres is a song queued, play it
        if (this.next() != null) {
            log.debug("Playing next song " + this.next().toString());
            this.previousAudioSource = this.current();
            this.currentAudioSource = this.next();
            this.nextAudioSource = this.getNext();

            // Add to played songs
            this.playedAudioSources.add(this.previous());
            // Remove the next song from upcoming
            this.upcomingAudioSources.remove(this.next());
            return this.current();
        } else {
            // Reset
            this.previousAudioSource = null;
            this.currentAudioSource = null;

            // If there are no more songs upcoming
            if (this.upcomingAudioSources.isEmpty()) {
                // If we are not repeating the list, send a failed error, as it is empty
                if (!this.isRepeating()) {
                    // TODO: Add PlaylistEmptyEvent
                    log.error("Playlist is empty");
                    return null;
                } else {
                    log.error("Playlist is empty! Resetting...");
                    this.reset();
                    return this.playNext();
                }
            } else {
                // There is no song queued up, queue one then recall this method
                this.nextAudioSource = this.getNext();
                // Remove the next song from upcoming
                this.upcomingAudioSources.remove(this.next());
                return this.playNext();
            }
        }
    }

    @Override
    public void stop() {
        this.previousAudioSource = this.currentAudioSource;
    }

    public void addSong(AudioSource audioSource) {
        if (this.exists(audioSource)){
            log.error("AudioSource already exists! " + audioSource.toString());
        return;}

        fullList.add(audioSource);
        upcomingAudioSources.add(audioSource);
    }

    public void removeSong(AudioSource audioSource) {
        if (!this.exists(audioSource))
            log.error("AudioSource does not exist! " + audioSource.toString());
        else
            fullList.remove(audioSource);
    }

    public boolean exists(AudioSource audioSource) {
        for (AudioSource audioSource1 : fullList)
            if (audioSource1.toString().equals(audioSource.toString()))
                return true;
        return false;
    }

    public AudioSource getSongFromName(String name, boolean partial) {
        for (AudioSource audioSource : this.fullList){
            if (audioSource.toString().toLowerCase().contains(name.toLowerCase()))
                return audioSource;
        }
        return null;
    }

    @Override
    public AudioSource getNext() {
        if (this.upcomingAudioSources.size() == 0)
            return null;
        if (this.upcomingAudioSources.size() == 1)
            return this.upcomingAudioSources.get(0);
        int i = 0;
        if (this.isShuffled())
            i = new Random().nextInt(this.upcomingAudioSources.size());
        AudioSource audioSource = this.upcomingAudioSources.get(i);
        log.debug("getNext " + audioSource.toString());
        return audioSource;
    }

    @Override
    public void reset() {
        this.previousAudioSource = null;
        this.currentAudioSource = null;
        this.nextAudioSource = null;
        this.playedAudioSources.clear();
        this.upcomingAudioSources.clear();
        this.upcomingAudioSources.addAll(this.fullList);
    }

    @Override
    public int size() {
        return this.fullList.size();
    }

    public void shuffle(boolean shuffle) {
        this.shuffle = shuffle;
    }

    public boolean isShuffled() {
        return this.shuffle;
    }

    public void repeat(boolean repeat) {
        this.repeat = repeat;
    }

    public boolean isRepeating() {
        return this.repeat;
    }
}
