/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.music.playlist;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Brock on 12/07/2016.
 */
public class YouTubePlaylist extends BasicPlaylist {

    private Logger log = LogManager.getLogger(YouTubePlaylist.class);

    public YouTubePlaylist() {

    }

    public void addPlaylist(String playlist) {
        if (playlist.contains("http")) {
            try {
                URL url = new URL(playlist);
                playlist = url.getQuery();
                playlist = playlist.substring(playlist.indexOf("list=") + 5);
                if (playlist.contains("&")) playlist = playlist.substring(0, playlist.indexOf("&"));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        YTPlaylistProcessor processor = new YTPlaylistProcessor(this, playlist);
        Thread thread = new Thread(processor);
        thread.setName("YTPlaylistProcessor-" + playlist);
        thread.start();
    }
}
