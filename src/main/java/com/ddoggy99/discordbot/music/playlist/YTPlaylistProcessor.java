/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.music.playlist;

import com.ddoggy99.discordbot.config.MusicConfig;
import com.ddoggy99.discordbot.music.AudioSource;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Brock on 13/07/2016.
 */
public class YTPlaylistProcessor implements Runnable {

    private YouTubePlaylist playlist;
    private String code;

    public YTPlaylistProcessor(YouTubePlaylist playlist, String code){
        this.playlist = playlist;
        this.code = code;
    }

    @Override
    public void run() {
        ArrayList<String> list = new ArrayList<>();
        list.addAll(MusicConfig.YOUTUBEDL_PLAYLIST_INFO);
        list.add(this.code);
        Process process = null;
        try {
            // TODO: Move to new thread
            process = new ProcessBuilder().command(list).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (process == null)
            throw new RuntimeException("Unable to start process");
        Scanner scanner = new Scanner(process.getInputStream());
        JSONArray json = new JSONArray();
        while (scanner.hasNext()){
            JSONObject source = new JSONObject(scanner.nextLine());
            json.put(source);
        }
        for (int i = 0; i < json.length(); i++){
            JSONObject source = json.getJSONObject(i);
            AudioSource audioSource = AudioSource.createSource(source.getString("id"), source.getString("playlist_title"), source.getString("uploader"), source.getString("title"));
            this.playlist.addSong(audioSource);
        }
    }
}
