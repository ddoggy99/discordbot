/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.music.station;

import com.ddoggy99.discordbot.music.AudioSource;
import com.ddoggy99.discordbot.music.State;
import com.ddoggy99.discordbot.music.playlist.IPlaylist;
import net.dv8tion.jda.audio.AudioSendHandler;

/**
 * Created by Brock on 1/07/2016.
 */
public interface IStation extends AudioSendHandler, Runnable {

    void request(AudioSource audioSource);

    void start();

    void stop();

    void pause();

    boolean isPlaying();

    boolean isStopped();

    boolean isPaused();

    boolean isFinished();
    boolean isPlaylistEmpty();

    String getName();

    String getType();

    State getState();

    IPlaylist getPlaylist();

    void setPlaylist(IPlaylist playlist);

    void setVolume(int volume);
}
