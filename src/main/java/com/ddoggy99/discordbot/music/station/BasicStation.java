/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.music.station;

import com.ddoggy99.discordbot.DiscordBot;
import com.ddoggy99.discordbot.events.jda.MusicPlayEvent;
import com.ddoggy99.discordbot.music.AudioSource;
import com.ddoggy99.discordbot.music.AudioStream;
import com.ddoggy99.discordbot.music.State;
import com.ddoggy99.discordbot.music.playlist.IPlaylist;
import net.dv8tion.jda.audio.AudioConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Arrays;

/**
 * Created by Brock on 1/07/2016.
 */
public class BasicStation implements IStation {

    private static final int PCM_FRAME_SIZE = 4;
    private Logger log = LogManager.getLogger(this);
    private byte[] buffer = new byte[AudioConnection.OPUS_FRAME_SIZE * PCM_FRAME_SIZE];

    private float volume = 0.1F;

    private String name = null;
    private IPlaylist playlist;
    private AudioStream currentAudioStream = null;

    private State state = State.UNKNOWN;

    public BasicStation(String name) {
        this( name, null);
    }

    public BasicStation(String name, IPlaylist playlist) {
        if (name == null || name.length() == 0)
            throw new IllegalArgumentException("Station must have a name!");
        this.name = name;
        this.playlist = playlist;
    }

    @Override
    public void request(AudioSource audioSource) {
        this.currentAudioStream = audioSource.asStream();
        this.state = State.PLAYING;
        DiscordBot.getEventManager().handle(new MusicPlayEvent(DiscordBot.getControlChannelID(), audioSource));
    }

    @Override
    public void start() {
        this.state = State.STARTING;
    }

    @Override
    public void stop() {
        this.state = State.STOPPED;
        // TODO: When requested to stop, stop playing and remove the song
        this.currentAudioStream = null;
    }

    @Override
    public void pause() {
        this.state = State.PAUSED;
    }

    @Override
    public boolean canProvide() {
        return this.state.equals(State.PLAYING);
    }

    @Override
    public byte[] provide20MsAudio() {
        try {
            int amountRead = currentAudioStream.read(buffer, 0, buffer.length);
            if (amountRead > -1) {
                if (amountRead < buffer.length) {
                    Arrays.fill(buffer, amountRead, buffer.length - 1, (byte) 0);
                }
                if (volume != 1) {
                    short sample;
                    for (int i = 0; i < buffer.length; i += 2) {
                        sample = (short) ((buffer[i + 1] & 0xff) | (buffer[i] << 8));
                        sample = (short) (sample * volume);
                        buffer[i + 1] = (byte) (sample & 0xff);
                        buffer[i] = (byte) ((sample >> 8) & 0xff);
                    }
                }
                //System.out.println("Buffer: " + buffer.length);
                return buffer;
            } else {
                this.state = State.FINISHED;
                return null;
            }
        } catch (IOException e) {
            this.state = State.STOPPED;
        }
        return null;
    }

    @Override
    public boolean isPlaying() {
        return this.getState().equals(State.PLAYING);
    }

    @Override
    public boolean isStopped() {
        return this.getState().equals(State.STOPPED);
    }

    @Override
    public boolean isPaused() {
        return this.getState().equals(State.PAUSED);
    }

    @Override
    public boolean isFinished() {
        return this.getState().equals(State.FINISHED);
    }

    @Override
    public boolean isPlaylistEmpty() {
        return this.getState().equals(State.EMPTY_PLAYLIST);
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getType() {
        return "File";
    }

    @Override
    public State getState() {
        return this.state;
    }

    @Override
    public IPlaylist getPlaylist() {
        return this.playlist;
    }

    @Override
    public void setPlaylist(IPlaylist playlist) {
        this.playlist = playlist;
    }

    @Override
    public void setVolume(int volume) {
        this.volume = volume / 100;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            if (this.isPlaylistEmpty() && this.getPlaylist().size() >= 1) {
                log.debug("Playlist was empty but is no longer empty, STARTING");
                this.state = State.STARTING;
            }
            // Play first song
            if (this.isFinished() || this.getState().equals(State.STARTING)) {
                AudioSource audioSource = this.getPlaylist().playNext();
                if (audioSource == null){
                    // If there is no audio source
                    // The playlist could be empty/not yet populated
                    this.state = State.EMPTY_PLAYLIST;
                    continue;
                }

                DiscordBot.getEventManager().handle(new MusicPlayEvent(DiscordBot.getControlChannelID(), audioSource));

                log.debug("Playing audioSource " + audioSource.toString());
                this.currentAudioStream = audioSource.asStream();
                this.state = State.PLAYING;
            }
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
