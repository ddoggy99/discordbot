/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.music;

import com.ddoggy99.discordbot.util.FileUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Brock on 30/06/2016.
 */
public class MusicDiscoverer {

    /**
     * List of possible music locations
     */
    private static ArrayList<String> possibleLocations = new ArrayList<>();
    private static List<String> FILE_EXTENSIONS = Arrays.asList("MP3", "M4A");

    private Logger log = LogManager.getLogger(this);
    private ArrayList<String> allowedArtists = new ArrayList<>();
    private ArrayList<String> allowedAlbums = new ArrayList<>();
    private ArrayList<String> allowedSongs = new ArrayList<>();

    private boolean partialMatchAllowed = true;

    public static void addPossibleLocation(String location) {
        MusicDiscoverer.addPossibleLocation(new File(location));
    }

    public static void addPossibleLocation(File file) {
        if (!file.exists())
            throw new IllegalArgumentException("Given path does not exist! " + file.getAbsolutePath());
        if (!file.isDirectory())
            throw new IllegalArgumentException("Given path is not a directory! " + file.getAbsolutePath());
        if (!file.canRead())
            throw new IllegalArgumentException("Can not read the given path! " + file.getAbsolutePath());
        possibleLocations.add(file.getAbsolutePath());
    }

    public static ArrayList<String> getPossibleLocations() {
        return possibleLocations;
    }

    public ArrayList<AudioSource> discover() {
        log.info("Discovering music from the following paths: ");
        int i = 0;
        for (String path : possibleLocations)
            log.info((++i) + " - '" + path + "'");
        i = 0;
        ArrayList<String> all = new ArrayList<>();
        for (String path : possibleLocations) {
            i++;
            log.info("Discovering from path " + i);
            all.addAll(FileUtil.searchFolder(path));
            //log.debug(FileUtil.searchFolder(path));
        }
        log.info("Discovered ~" + all.size() + " files");
        log.info("Scanning list for music files");
        for (String EXTENSION : FILE_EXTENSIONS)
            log.info("Looking for " + EXTENSION + " file extension");
        File file;
        ArrayList<File> musicFiles = new ArrayList<>();
        for (String filePath : all) {
            file = new File(filePath);
            for (String EXTENSION : FILE_EXTENSIONS)
                if (file.getName().toLowerCase().endsWith("." + EXTENSION.toLowerCase())) {
                    log.debug("Found a music file " + file.getAbsolutePath());
                    musicFiles.add(file);
                }
        }
        log.info("Found " + musicFiles.size() + " music files!");

        boolean allowed;
        boolean artistAllowed;
        boolean albumAllowed;
        boolean songAllowed;

        ArrayList<AudioSource> audioSourceList = new ArrayList<>();

        for (File musicFile : musicFiles) {
            String fileName = musicFile.getName();
            // Remove the last 4 things as that is the file extension
            fileName = fileName.substring(0, fileName.length() - 4);
            File albumFolder = new File(musicFile.getParent());
            File artistFolder = new File(albumFolder.getParent());
            artistAllowed = allowedArtists.contains(artistFolder.getName());
            albumAllowed = allowedAlbums.contains(albumFolder.getName());
            songAllowed = allowedSongs.contains(musicFile.getName());
            if (this.isPartialMatchAllowed())
                allowed = (artistAllowed || albumAllowed || songAllowed);
            else
                allowed = (artistAllowed && albumAllowed && songAllowed);

            if (allowed)
                audioSourceList.add(AudioSource.createSource("file", artistFolder.getName(), albumFolder.getName(), fileName, musicFile.getAbsolutePath()));
        }
        log.info("Have " + audioSourceList.size() + " music files available");

        return audioSourceList;
    }

    public ArrayList<String> getAllowedSongs() {
        return allowedSongs;
    }

    public void setAllowedSongs(ArrayList<String> allowedSongs) {
        this.allowedSongs = allowedSongs;
    }

    public ArrayList<String> getAllowedArtists() {
        return allowedArtists;
    }

    public void setAllowedArtists(ArrayList<String> allowedArtists) {
        this.allowedArtists = allowedArtists;
    }

    public ArrayList<String> getAllowedAlbums() {
        return allowedAlbums;
    }

    public void setAllowedAlbums(ArrayList<String> allowedAlbums) {
        this.allowedAlbums = allowedAlbums;
    }

    public boolean isPartialMatchAllowed() {
        return partialMatchAllowed;
    }

    public void setPartialMatchAllowed(boolean partialMatchAllowed) {
        this.partialMatchAllowed = partialMatchAllowed;
    }

}
