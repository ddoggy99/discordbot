/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.music;

/**
 * Created by Brock on 1/07/2016.
 */
public enum State {
    UNKNOWN,
    NEVER_STARTED, // Default state
    STARTING, // If it has been asked to start but not yet playing anything
    PLAYING, // If it currently playing something
    PAUSED, // If the station has been paused
    STOPPED, // If the station has been stopped, meaning their is no song selected
    FINISHED, // If the current song has finished and not yet been set to a new song
    EMPTY_PLAYLIST // If the current playlist is empty or no playlist is defined!
}
