/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.music;

import com.ddoggy99.discordbot.config.MusicConfig;

import java.io.File;
import java.util.ArrayList;

/**
 * Repersents a single song
 * <p>
 * Created by Brock on 4/06/2016.
 */
public class AudioSource {

    private static int number = -1;
    private int id;
    private String type;
    private String artist;
    private String album;
    private String song;
    private String source;
    private AudioStream stream;

    private AudioSource(int id, String type, String artist, String album, String song, String source) {
        this.id = id;
        this.type = type;
        this.artist = artist;
        this.album = album;
        this.song = song;
        this.source = source;
    }

    public static AudioSource createSource(String ytid) {
        // TODO: Allow using channel name as artist
        return createSource("youtube", null, null, ytid, ytid);
    }

    public static AudioSource createSource(String ytid, String playlistTitle, String uploader, String title) {
        // TODO: Allow using channel name as artist
        return createSource("youtube", uploader, playlistTitle, title, ytid);
    }

    public static AudioSource createSource(String type, String artist, String album, String song, String source) {
        return createSource(number++, type, artist, album, song, source);
    }

    public static AudioSource createSource(int id, String type, String artist, String album, String song, String source) {
        return new AudioSource(id, type, artist, album, song, source);
    }

    public AudioStream asStream() {
        //if (this.stream == null) {
        if (this.getType().equalsIgnoreCase("file")) {
            ArrayList<String> args = new ArrayList<>();
            args.addAll(MusicConfig.FFMPEG_STREAM_ARGS);
            args.add(new File(this.getSource()).getAbsolutePath());
            this.stream = new FileAudioStream(args);
        } else if (this.getType().equalsIgnoreCase("youtube")) {
            ArrayList<String> ytArgs = new ArrayList<>();
            ytArgs.addAll(MusicConfig.YOUTUBEDL_STREAM_ARGS);
            ytArgs.add("https://www.youtube.com/watch?v=" + this.getSource());
            ArrayList<String> ffmpegArgs = new ArrayList<>();
            ffmpegArgs.addAll(MusicConfig.FFMPEG_YTDL_STREAM_ARGS);
            this.stream = new YTAudioStream(ytArgs, ffmpegArgs);
        }
        //}
        return this.stream;
    }

    public int getID() {
        return this.id;
    }

    public String getType() {
        return this.type;
    }

    public String getArtist() {
        return this.artist;
    }

    public String getAlbum() {
        return this.album;
    }

    public String getSong() {
        return this.song;
    }

    public String getSource() {
        return this.source;
    }

    @Override
    public String toString() {
        return this.getID() + ":" + this.getArtist() + ":" + this.getAlbum() + ":" + this.getSong();
    }
}

