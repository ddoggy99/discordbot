/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.music;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;

/**
 * Based on code from JDA Player
 * https://github.com/DV8FromTheWorld/JDA-Player/
 * https://github.com/DV8FromTheWorld/JDA-Player/blob/master/src/main/java/net/dv8tion/jda/player/source/RemoteStream.java
 * <p>
 * Created by Brock on 3/07/2016.
 */
public class YTAudioStream extends AudioStream {

    private Logger log = LogManager.getLogger(this);

    private Process ytdlProcess;
    private Process ffmpegProcess;

    private Thread bridgeThread;
    private Thread ytdlGobler;
    private Thread ffmpegGobler;

    public YTAudioStream(List<String> ytdlArgs, List<String> ffmpegArgs) {
        try {
            ProcessBuilder builder = new ProcessBuilder();
            ytdlProcess = builder.command(ytdlArgs).start();
            ffmpegProcess = builder.command(ffmpegArgs).start();

            String temp = ytdlArgs.get(ytdlArgs.size() - 1);
            temp = temp.substring(temp.indexOf("v=") + 2);
            final String ytCode = temp;

            bridgeThread = new Thread("YTDL to FFMPEG Bridge " + ytCode) {
                @Override
                public void run() {
                    InputStream ytdlIn = ytdlProcess.getInputStream();
                    OutputStream ffmpegOut = ffmpegProcess.getOutputStream();
                    try {
                        byte[] buffer = new byte[1024];
                        int amountRead = -1;
                        while (!isInterrupted() && ((amountRead = ytdlIn.read(buffer)) > -1))
                            ffmpegOut.write(buffer, 0, amountRead);
                        ffmpegOut.flush();
                    } catch (IOException e) {
                        if (!e.getMessage().contains("The pipe has been ended"))
                            e.printStackTrace();
                    } finally {
                        if (ytdlIn != null)
                            try {
                                ytdlIn.close();
                            } catch (IOException ignored) {
                            }
                        if (ffmpegOut != null)
                            try {
                                ffmpegOut.close();
                            } catch (IOException ignored) {
                            }
                    }
                }
            };
            ytdlGobler = new Thread("YTDL ErrorGobler " + ytCode) {
                @Override
                public void run() {
                    try {
                        InputStream in = ytdlProcess.getErrorStream();
                        byte[] buffer = new byte[1024];
                        int amountRead = -1;
                        while (!isInterrupted() && ((amountRead = in.read(buffer)) > -1))
                            log.error(new String(Arrays.copyOf(buffer, amountRead)));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            };

            ffmpegGobler = new Thread("FFMPEG ErrorGobler " + ytCode) {
                @Override
                public void run() {
                    try {
                        InputStream in = ffmpegProcess.getErrorStream();
                        byte[] buffer = new byte[1024];
                        int amountRead = -1;
                        while (!isInterrupted() && ((amountRead = in.read(buffer)) > -1)){
                            /*String read = new String(Arrays.copyOf(buffer, amountRead));
                            if (read.length() >= 3 && !read.contains("size="))
                            log.error(read);*/
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            };
            ytdlGobler.start();
            ffmpegGobler.start();
            bridgeThread.start();
            this.in = ffmpegProcess.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
            try
            {
                close();
            }
            catch (IOException e1)
            {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public void close() throws IOException {
        if (in != null) {
            in.close();
            in = null;
        }
        if (bridgeThread != null) {
            bridgeThread.interrupt();
            bridgeThread = null;
        }
        if (ytdlGobler != null) {
            ytdlGobler.interrupt();
            ytdlGobler = null;
        }
        if (ffmpegGobler != null) {
            ffmpegGobler.interrupt();
            ffmpegGobler = null;
        }
        if (ffmpegProcess != null) {
            ffmpegProcess.destroy();
            ffmpegProcess = null;
        }
        if (ytdlProcess != null) {
            ytdlProcess.destroy();
            ytdlProcess = null;
        }
        super.close();
    }
}
