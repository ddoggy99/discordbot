/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.commands;

import com.ddoggy99.discordbot.DiscordBot;
//import com.ddoggy99.discordbot.handlers.MusicHandler;
import com.ddoggy99.discordbot.config.Config;
import com.ddoggy99.discordbot.events.CommandNotImplementedEvent;
import com.ddoggy99.discordbot.util.ChannelUtil;
import com.ddoggy99.discordbot.util.MessageUtil;
import net.dv8tion.jda.entities.Guild;
import net.dv8tion.jda.entities.Message;
import net.dv8tion.jda.entities.MessageChannel;
import net.dv8tion.jda.entities.TextChannel;
import net.dv8tion.jda.managers.AudioManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Brock on 18/06/2016.
 */
public class CommandLeave implements ICommand {

    private Logger log;

    public CommandLeave(){
        log = LogManager.getLogger(this);
    }
    @Override
    public String getCommand() {
        return "leave";
    }

    @Override
    public String getDescription() {
        return "Leaves the currently connected channel";
    }

    @Override
    public String getHelp() {
        return "!leave";
    }

    @Override
    public List<String> getAliases() {
        return Collections.singletonList("l");
    }

    @Override
    public void onCommand(Message message, String args) {
        Guild guild = ChannelUtil.getGuildOfTChannel(message.getChannelId());
        if (guild == null){
            log.fatal("Unable to get guild of channel!");
            throw new RuntimeException();
        }
        AudioManager audioManager = guild.getAudioManager();
        if (audioManager.isAttemptingToConnect()){
            MessageUtil.sendMessageAsync(message.getChannelId(), "Still connecting to channel, Please wait!");
            return;
        }
        if (!audioManager.isConnected()){
            MessageUtil.sendMessageAsync(message.getChannelId(), "Not connected to a channel!");
            return;
        }
        if (Config.CURRENT_STATION != null && Config.CURRENT_STATION.isPlaying()) Config.CURRENT_STATION.pause();
        audioManager.closeAudioConnection();
    }
}
