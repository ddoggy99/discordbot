/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.commands;

import com.ddoggy99.discordbot.DiscordBot;
//import com.ddoggy99.discordbot.events.CommandFailedEvent;
//import com.ddoggy99.discordbot.utils.MessageUtil;
import com.ddoggy99.discordbot.events.CommandNotImplementedEvent;
import com.ddoggy99.discordbot.util.ChannelUtil;
import com.ddoggy99.discordbot.util.MessageUtil;
import com.ddoggy99.discordbot.util.UserUtil;
import net.dv8tion.jda.entities.Message;
import net.dv8tion.jda.entities.User;
import net.dv8tion.jda.entities.VoiceChannel;
import net.dv8tion.jda.managers.AudioManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by Brock on 17/06/2016.
 */
public class CommandJoin implements ICommand {

    private Logger log = LogManager.getLogger(this);

    @Override
    public String getCommand() {
        return "join";
    }

    @Override
    public String getDescription() {
        return "Joins a given voice channel or executors current channel if connected";
    }

    @Override
    public String getHelp() {
        return "!join (channel)";
    }

    @Override
    public List<String> getAliases() {
        return Collections.singletonList("j");
    }

    @Override
    public void onCommand(Message message, String args) {
        VoiceChannel voiceChannel = null;

        if (args != null) {
            voiceChannel = ChannelUtil.getVoiceChannelByName(args, true);
        }
        if (voiceChannel == null) {
            voiceChannel = UserUtil.getConnectedVoiceChannel(message.getAuthor().getId());
        }
        if (voiceChannel != null) {
            AudioManager audioManager = voiceChannel.getGuild().getAudioManager();
            if (audioManager.isConnected()) {
                MessageUtil.sendMessageAsync(message.getChannelId(), "Switching to channel " + voiceChannel.getName());
                audioManager.moveAudioConnection(voiceChannel);
            } else {
                MessageUtil.sendMessageAsync(message.getChannelId(), "Connecting to channel " + voiceChannel.getName());
                audioManager.openAudioConnection(voiceChannel);
            }
        } else {
            if (args != null)
                MessageUtil.sendMessageAsync(message.getChannelId(), "Unknown channel! " + args);
            else
                MessageUtil.sendMessageAsync(message.getChannelId(), "You are not in a voice channel! Please specify a channel");
        }
    }
}
