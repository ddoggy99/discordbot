/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.commands;

import com.ddoggy99.discordbot.config.Config;
import com.ddoggy99.discordbot.handlers.CommandHandler;
import com.ddoggy99.discordbot.util.MessageUtil;
import com.ddoggy99.discordbot.util.VersionUtil;
import net.dv8tion.jda.entities.Message;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Brock on 18/06/2016.
 */
public class CommandHelp implements ICommand {

    @Override
    public String getCommand() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Prints the help menu";
    }

    @Override
    public String getHelp() {
        return "!help";
    }

    @Override
    public List<String> getAliases() {
        return Arrays.asList("h", "?");
    }

    @Override
    public void onCommand(Message message, String args) {
        ArrayList<ICommand> commandList = CommandHandler.getCommandList();
        StringBuilder builder = new StringBuilder();
        builder.append("Commands: \n");
        for (ICommand command : commandList) {
            builder.append(Config.LIST_SPACING + command.getHelp() + " - " + command.getDescription());
            builder.append("\n");
        }
        builder.append(Config.LIST_SPACING + "NOTE: Currently using music from a folder, i will add the ability to use youtube hopefully soon\n");
        builder.append(Config.LIST_SPACING + "Version: " + VersionUtil.VERSION + " ALPHA - STILL IN DEVELOPMENT AND MAY CRASH OR CAUSE DEAFNESS!");

        MessageUtil.sendMessageAsync(message.getChannelId(), builder.toString());
    }
}
