/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.commands;

import com.ddoggy99.discordbot.DiscordBot;
import com.ddoggy99.discordbot.config.Config;
import com.ddoggy99.discordbot.events.CommandNotImplementedEvent;
import com.ddoggy99.discordbot.util.MessageUtil;
import net.dv8tion.jda.entities.Message;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Brock on 4/07/2016.
 */
public class CommandTag implements ICommand {

    private ArrayList<String> aliases = new ArrayList<>();
    private HashMap<String, String> tags = new HashMap<>();

    public CommandTag() {
        this.aliases.add("t");
        this.aliases.add("tags");
        this.aliases.add("to");
        this.aliases.add("tago");
        this.tags.putAll(Config.DEFAULT_TAGS);
    }

    @Override
    public String getCommand() {
        return "tag";
    }

    @Override
    public String getDescription() {
        return "Adds tags which can store information (NOT FULLY IMPLEMENTED!)";
    }

    @Override
    public String getHelp() {
        return "!tag <name> [data]";
    }

    @Override
    public List<String> getAliases() {
        /*ArrayList<String> list = new ArrayList<>();
        list.addAll(this.aliases);
        list.addAll(this.tags.keySet());*/
        return this.aliases;
    }

    @Override
    public void onCommand(Message message, String args) {
        if (message.getContent().startsWith("!tags")) {
            StringBuilder builder = new StringBuilder();
            builder.append("Current tags: \n");
            for (String key : this.tags.keySet()) {
                builder.append(Config.LIST_SPACING);
                builder.append(key);
                builder.append("\n");
            }
            MessageUtil.sendMessageAsync(message.getChannelId(), builder.toString());
            return;
        }

        boolean override = false;
        if (message.getContent().startsWith("!to") || message.getContent().startsWith("!tago")) {
            // TODO: Only allow based on tag ownership
            override = true;
        }
        if (!args.contains(" ")) {
            if (this.tags.containsKey(args))
                MessageUtil.sendMessageAsync(message.getChannelId(), this.tags.get(args));
            else
                MessageUtil.sendMessageAsync(message.getChannelId(), "Unknown tag " + args);
        } else {
            //MessageUtil.sendMessageAsync(message.getChannelId(), "Unable to create tags yet!");
            String[] split = args.split(" ", 2);
            if (this.tags.containsKey(split[0])) {
                if (!override) {
                    MessageUtil.sendMessageAsync(message.getChannelId(), "Unable to override tag " + split[0]);
                } else {
                    this.tags.put(split[0], split[1]);
                    MessageUtil.sendMessageAsync(message.getChannelId(), "Override tag '" + split[0] + "' to '" + split[1] + "'");
                }
            } else {
                // TODO: Disallow using tag names that are commands
                this.tags.put(split[0], split[1]);
                MessageUtil.sendMessageAsync(message.getChannelId(), "Set tag '" + split[0] + "' to '" + split[1] + "'");
            }
        }
    }
}
