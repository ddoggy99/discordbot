/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.commands;

import com.ddoggy99.discordbot.DiscordBot;
import com.ddoggy99.discordbot.config.Config;
import com.ddoggy99.discordbot.db.MySQL;
//import com.ddoggy99.discordbot.events.FeedbackReceivedEvent;
//import com.ddoggy99.discordbot.utils.MessageUtil;
import com.ddoggy99.discordbot.events.CommandNotImplementedEvent;
import net.dv8tion.jda.entities.Message;
import net.dv8tion.jda.entities.PrivateChannel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Brock on 18/06/2016.
 */
public class CommandFeedback implements ICommand {
    @Override
    public String getCommand() {
        return "feedback";
    }

    @Override
    public String getDescription() {
        return "Allows giving feedback to the bot developers";
    }

    @Override
    public String getHelp() {
        return "!feedback [feedback]";
    }

    @Override
    public List<String> getAliases() {
        return null;
    }

    @Override
    public void onCommand(Message message, String args) {
        DiscordBot.getEventManager().handle(new CommandNotImplementedEvent(DiscordBot.getJDA(), message));/*
        if (args == null || args.length() == 0) {
            MessageUtil.sendMessageAsync(message.getChannelId(), "Please specify feedback!");
            return;
        }

        if (args.equalsIgnoreCase("list")) {
            if (!message.getAuthor().getId().equalsIgnoreCase(Config.DEVELOPER_ID)) {
                MessageUtil.sendMessageAsync(message.getChannelId(), "You must be a developer to use this commands");
                return;
            }
            // TODO: List feedback
            System.out.println("Sending feedback to developer!");
            PrivateChannel devChan = DiscordBot.getJDA().getUserById(Config.DEVELOPER_ID).getPrivateChannel();
            ArrayList<HashMap> results = MySQL.executeQuery("SELECT username, content FROM discord_bot.feedback, discord_bot.users WHERE `feedback`.`reporterid` = `users`.`id`;");
            if (results == null || results.isEmpty()) {
                devChan.sendMessageAsync("No feedback!", null);
                return;
            }

            StringBuilder builder = new StringBuilder();
            for (HashMap map : results) {
                builder.append(map.get("username"));
                builder.append(": ");
                builder.append(map.get("content"));
                builder.append("\n");
            }
            devChan.sendMessageAsync(builder.toString(), null);
        } else {
            DiscordBot.getEventManager().handle(new FeedbackReceivedEvent(DiscordBot.getJDA(), 200, message));
        }*/
    }
}
