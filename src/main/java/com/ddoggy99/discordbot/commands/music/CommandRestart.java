/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.commands.music;

import com.ddoggy99.discordbot.DiscordBot;
import com.ddoggy99.discordbot.commands.ICommand;
//import com.ddoggy99.discordbot.handlers.MusicHandler;
//import com.ddoggy99.discordbot.utils.MessageUtil;
import com.ddoggy99.discordbot.events.CommandNotImplementedEvent;
import net.dv8tion.jda.entities.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Brock on 18/06/2016.
 */
public class CommandRestart implements ICommand {

    private Logger log = LogManager.getLogger(this);

    @Override
    public String getCommand() {
        return "restart";
    }

    @Override
    public String getDescription() {
        return "Restarts the queue from the beginning";
    }

    @Override
    public String getHelp() {
        return "!restart";
    }

    @Override
    public List<String> getAliases() {
        return Arrays.asList("reset");
    }

    @Override
    public void onCommand(Message message, String args) {
        DiscordBot.getEventManager().handle(new CommandNotImplementedEvent(DiscordBot.getJDA(), message));/*
        MessageUtil.sendMessageAsync(message.getChannelId(), "Restarting music queue!");
        log.info("Restarting music queue");
        MusicHandler.restartMusic();*/
    }
}
