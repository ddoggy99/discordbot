/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.commands.music;

import com.ddoggy99.discordbot.commands.ICommand;
import com.ddoggy99.discordbot.config.Config;
import com.ddoggy99.discordbot.util.MessageUtil;
import net.dv8tion.jda.entities.Message;

import java.util.Collections;
import java.util.List;

/**
 * Created by Brock on 13/07/2016.
 */
public class CommandVolume implements ICommand{
    @Override
    public String getCommand() {
        return "volume";
    }

    @Override
    public String getDescription() {
        return "Sets the stations volume, from 0-100";
    }

    @Override
    public String getHelp() {
        return "!volume [amount]";
    }

    @Override
    public List<String> getAliases() {
        return Collections.singletonList("v");
    }

    @Override
    public void onCommand(Message message, String args) {
        int amount;
        if (args == null){
            MessageUtil.sendMessageAsync(message.getChannelId(), "Please specify volume");
            return;
        } else {
            try {
                amount = Integer.parseInt(args);
            } catch (NumberFormatException e){
                MessageUtil.sendMessageAsync(message.getChannelId(), "Invalid volume " + args);
                return;
            }
        }
        if (Config.CURRENT_STATION == null){
            MessageUtil.sendMessageAsync(message.getChannelId(), "No station defined!");
            return;
        }
        MessageUtil.sendMessageAsync(message.getChannelId(), "Setting volume to " + amount);
        Config.CURRENT_STATION.setVolume(amount);
    }
}
