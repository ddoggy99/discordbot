/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.commands.music;

import com.ddoggy99.discordbot.commands.ICommand;
import com.ddoggy99.discordbot.config.Config;
import com.ddoggy99.discordbot.music.station.IStation;
import com.ddoggy99.discordbot.util.MessageUtil;
import net.dv8tion.jda.entities.Message;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Brock on 3/07/2016.
 */
public class CommandStations implements ICommand {
    @Override
    public String getCommand() {
        return "stations";
    }

    @Override
    public String getDescription() {
        return "Lists all stations";
    }

    @Override
    public String getHelp() {
        return "!stations";
    }

    @Override
    public List<String> getAliases() {
        return null;
    }

    @Override
    public void onCommand(Message message, String args) {
        HashMap<String, IStation> stations = Config.STATIONS;
        if (stations.isEmpty()){
            MessageUtil.sendMessageAsync(message.getChannelId(), "No stations defined!");
            return;
        }
        StringBuilder builder = new StringBuilder();
        for (IStation station : stations.values())
            builder.append(Config.LIST_SPACING + station.getName() + " of type " + station.getType() + "\n");

        builder.insert(0, "Currently defined stations: \n");
        MessageUtil.sendMessageAsync(message.getChannelId(), builder.toString());
    }
}
