/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.commands.music;

import com.ddoggy99.discordbot.DiscordBot;
import com.ddoggy99.discordbot.commands.ICommand;
//import com.ddoggy99.discordbot.handlers.MusicHandler;
//import com.ddoggy99.discordbot.music.AudioSource;
//import com.ddoggy99.discordbot.utils.MessageUtil;
import com.ddoggy99.discordbot.config.Config;
import com.ddoggy99.discordbot.music.AudioSource;
import com.ddoggy99.discordbot.music.station.IStation;
import com.ddoggy99.discordbot.util.MessageUtil;
import net.dv8tion.jda.entities.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Brock on 18/06/2016.
 */
public class CommandPlay implements ICommand {

    private Logger log = LogManager.getLogger(this);

    @Override
    public String getCommand() {
        return "play";
    }

    @Override
    public String getDescription() {
        return "Starts the song queue or players the given song";
    }

    @Override
    public String getHelp() {
        return "!play (song)";
    }

    @Override
    public List<String> getAliases() {
        return Arrays.asList("start");
    }

    @Override
    public void onCommand(Message message, String args) {
        //DiscordBot.getEventManager().handle(new CommandNotImplementedEvent(DiscordBot.getJDA(), message));

        if (!DiscordBot.getAudioManager().isConnected()) {
            MessageUtil.sendMessageAsync(message.getChannelId(), "Not connected to any channel!");
            return;
        }
        if (DiscordBot.getAudioManager().isAttemptingToConnect()) {
            MessageUtil.sendMessageAsync(message.getChannelId(), "Please wait until I connect!");
            return;
        }
        if (DiscordBot.getAudioManager().getSendingHandler() == null || Config.CURRENT_STATION == null) {
            MessageUtil.sendMessageAsync(message.getChannelId(), "No station selected!");
            return;
        }

        IStation station = Config.CURRENT_STATION;

        if (station.isPlaying() && (args == null || args.length() <= 0)) {
            MessageUtil.sendMessageAsync(message.getChannelId(), "Already playing...");
            return;
        }
        if (station.isPlaylistEmpty()){
            MessageUtil.sendMessageAsync(message.getChannelId(), "Playlist empty");
            return;
        }

        if (args != null && args.length() > 0) {
            log.info("Trying to play " + args);
            AudioSource requestedAudioSource = station.getPlaylist().getSongFromName(args, true);
            if (requestedAudioSource == null) {
                MessageUtil.sendMessageAsync(message.getChannelId(), "Unknown song: " + args);
                return;
            }
            // TODO: Remove and replace with the event system in IStation#run
            MessageUtil.sendMessageAsync(message.getChannelId(), "Playing song: " + requestedAudioSource.getSong());
            log.info("Playing song: " + requestedAudioSource.getSong());
            station.stop();
            station.request(requestedAudioSource);
        } else
            station.start();


        /*
        String channelID = message.getChannelId();
        if (args != null) {
            log.debug("Playing requested song: " + args);
        }
        MessageUtil.sendMessageAsync(channelID, "Trying to playing...");
        MusicHandler.useFileQueue();
        MusicHandler.useFilePlayer();
        AudioSource song;
        if (args != null)
            song = MusicHandler.playMusic(args);
        else
            song = MusicHandler.playMusic(null);

        if (song != null)
            log.info("Playing song " + song.getAudioSource() + " by " + song.getArtist());
        else if (args != null)
            MessageUtil.sendMessageAsync(channelID, "Failed to play requested song! " + args);
        else
            MessageUtil.sendMessageAsync(channelID, "Failed to play song!");*/
    }
}
