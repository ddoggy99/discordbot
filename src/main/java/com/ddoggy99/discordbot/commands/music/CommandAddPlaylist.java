/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.commands.music;

import com.ddoggy99.discordbot.commands.ICommand;
import com.ddoggy99.discordbot.config.Config;
import com.ddoggy99.discordbot.music.playlist.YouTubePlaylist;
import com.ddoggy99.discordbot.music.station.YouTubeStation;
import com.ddoggy99.discordbot.util.MessageUtil;
import net.dv8tion.jda.entities.Message;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Brock on 11/07/2016.
 */
public class CommandAddPlaylist implements ICommand {

    @Override
    public String getCommand() {
        return "addplaylist";
    }

    @Override
    public String getDescription() {
        return "Creates and adds a YouTube playlist to a station";
    }

    @Override
    public String getHelp() {
        return "!addplaylist [name] [link]";
    }

    @Override
    public List<String> getAliases() {
        return Arrays.asList("addp", "ap", "addpl", "apl", "addlist", "alist");
    }

    @Override
    public void onCommand(Message message, String args) {
        String[] split = args.split(" ");
        if (split.length != 2){
            MessageUtil.sendMessageAsync(message.getChannelId(), "Invalid arguments!");
            return;
        }
        String name = split[0];
        String link = split[1];
        if (Config.STATIONS.containsKey(name)) {
            MessageUtil.sendMessageAsync(message.getChannelId(), "Playlist already exists!");
            return;
        }
        MessageUtil.sendMessageAsync(message.getChannelId(), "Downloading playlist information");
        YouTubePlaylist playlist = new YouTubePlaylist();
        playlist.addPlaylist(link);
        YouTubeStation station = new YouTubeStation(name, playlist);
        Config.addStation(station);
        Thread thread = new Thread(station);
        thread.setName("Station-" + station.getName());
        thread.start();
        MessageUtil.sendMessageAsync(message.getChannelId(), "Successfully added playlist " + name + ", Please wait for the song information to download");
    }
}
