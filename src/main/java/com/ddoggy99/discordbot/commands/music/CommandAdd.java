/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.commands.music;

import com.ddoggy99.discordbot.commands.ICommand;
import com.ddoggy99.discordbot.config.Config;
import com.ddoggy99.discordbot.music.AudioSource;
import com.ddoggy99.discordbot.music.station.IStation;
import com.ddoggy99.discordbot.util.MessageUtil;
import net.dv8tion.jda.entities.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Brock on 3/07/2016.
 */
public class CommandAdd implements ICommand {

    private Logger log = LogManager.getLogger(this);

    @Override
    public String getCommand() {
        return "add";
    }

    @Override
    public String getDescription() {
        return "Adds a song to the given playlist or current one if none is specified";
    }

    @Override
    public String getHelp() {
        return "!add [source] (station)";
    }

    @Override
    public List<String> getAliases() {
        return null;
    }

    @Override
    public void onCommand(Message message, String args) {
        String[] pArgs = getArgs(message, args);
        if (pArgs == null)
            return;
        log.debug("Processing: " + Arrays.toString(pArgs));
        IStation station;
        if (pArgs.length == 2) {
            station = Config.STATIONS.get(pArgs[1]);
        } else {
            station = Config.CURRENT_STATION;
            if (station == null) {
                MessageUtil.sendMessageAsync(message.getChannelId(), "Unable to add video. No station is currently selected!");
                return;
            }
        }
        if (station == null) {
            log.error("STATION NULL!!!");
            return;
        } else if (station.getPlaylist() == null) {
            log.error("PLAYLIST IS NULL!!!");
            return;
        }
        AudioSource source = AudioSource.createSource(pArgs[0]);
        station.getPlaylist().addSong(source);
        log.debug("Added " + source.getSource() + " to station " + station.getName());
        MessageUtil.sendMessageAsync(message.getChannelId(), "Added " + source.getSource() + " to station " + station.getName());
    }

    private String[] getArgs(Message message, String args) {
        if (args == null || args.length() <= 0) {
            MessageUtil.sendMessageAsync(message.getChannelId(), "Invalid Arguments! " + args);
            return null;
        }

        String source = null;
        String playlist = null;

        if (args.contains(" ")) {
            String[] split = args.split(" ");
            if (split.length != 2) {
                MessageUtil.sendMessageAsync(message.getChannelId(), "Invalid Usage! " + this.getHelp());
                return null;
            } else {
                source = split[0];
                playlist = split[1];
            }
        } else {
            source = args;
        }

        if (source.contains("http") || source.contains("v=")) {
            try {
                URL url = new URL(args);
                String query = url.getQuery();
                if (query == null || !query.contains("v="))
                    throw new MalformedURLException();
                log.debug("QUERY: " + query);
                source = query.substring(query.indexOf("v=") + 2);
                source = source.substring(0, 11);
            } catch (MalformedURLException e) {
                MessageUtil.sendMessageAsync(message.getChannelId(), "Invalid URL Specified! " + args);
                return null;
            }
        }

        if (playlist != null) {
            if (!Config.STATIONS.containsKey(playlist)) {
                MessageUtil.sendMessageAsync(message.getChannelId(), "Unknown Playlist! " + playlist);
                return null;
            }
        }

        if (playlist != null)
            return new String[]{source, playlist};
        else
            return new String[]{source};
    }
}
