/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.commands.music;

import com.ddoggy99.discordbot.DiscordBot;
import com.ddoggy99.discordbot.commands.ICommand;
import com.ddoggy99.discordbot.config.Config;
import com.ddoggy99.discordbot.music.station.IStation;
import com.ddoggy99.discordbot.util.MessageUtil;
import net.dv8tion.jda.entities.Message;

import java.util.List;

/**
 * Created by Brock on 1/07/2016.
 */
public class CommandStation implements ICommand {
    @Override
    public String getCommand() {
        return "station";
    }

    @Override
    public String getDescription() {
        return "Sets the station to be used";
    }

    @Override
    public String getHelp() {
        return "!station [station]";
    }

    @Override
    public List<String> getAliases() {
        return null;
    }

    @Override
    public void onCommand(Message message, String args) {
        if (!Config.STATIONS.containsKey(args)) {
            MessageUtil.sendMessageAsync(message.getChannelId(), "Unknown station " + args);
            return;
        }
        MessageUtil.sendMessageAsync(message.getChannelId(), "Switching to station: " + args);
        IStation station = Config.STATIONS.get(args);
        boolean autoPlay = false;
        if (Config.CURRENT_STATION != null && Config.CURRENT_STATION.isPlaying()){
            autoPlay = true;
            Config.CURRENT_STATION.stop();
        }
        Config.CURRENT_STATION = station;
        DiscordBot.getAudioManager().setSendingHandler(station);
        if (autoPlay)
            Config.CURRENT_STATION.start();
    }
}
