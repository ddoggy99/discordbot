/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.commands.music;

import com.ddoggy99.discordbot.commands.ICommand;
import com.ddoggy99.discordbot.config.Config;
import com.ddoggy99.discordbot.music.station.IStation;
import com.ddoggy99.discordbot.util.MessageUtil;
import net.dv8tion.jda.entities.Message;

import java.util.List;

/**
 * Created by Brock on 13/07/2016.
 */
public class CommandStop implements ICommand {
    @Override
    public String getCommand() {
        return "stop";
    }

    @Override
    public String getDescription() {
        return "Stops the given station or current one if non is specified";
    }

    @Override
    public String getHelp() {
        return "!stop <station>";
    }

    @Override
    public List<String> getAliases() {
        return null;
    }

    @Override
    public void onCommand(Message message, String args) {
        IStation station = null;
        if (args != null) {
            if (!Config.STATIONS.containsKey(args)) {
                MessageUtil.sendMessageAsync(message.getChannelId(), "Unknown station " + args);
                return;
            } else {
                station = Config.STATIONS.get(args);
            }
        } else {
            if (Config.CURRENT_STATION != null){ station = Config.CURRENT_STATION;} else {
                MessageUtil.sendMessageAsync(message.getChannelId(), "No station selected!");
            }
        }
        if (station == null){
            throw new RuntimeException("Unable to get the station! Station: " + args);
        }
        if (station.isStopped()){
            MessageUtil.sendMessageAsync(message.getChannelId(), "Already stopped!");
            return;
        } else if (!station.isPlaying()){
            MessageUtil.sendMessageAsync(message.getChannelId(), "Not playing!");
            return;
        }
        station.pause();
        MessageUtil.sendMessageAsync(message.getChannelId(), "Stopped station " + station.getName());
    }
}
