/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.config;

import com.ddoggy99.discordbot.music.station.IStation;

import java.util.HashMap;

/**
 * Created by Brock on 9/06/2016.
 */
public class Config {

    // *********************************
    //        PREDEFINED FORMATTING
    // *********************************
    public static final String LIST_SPACING = "       - ";

    // *********************************
    //        DEVELOPER CONFIG
    // *********************************
    public static final String DEVELOPER_ID = "156582191906684928";
    // *********************************
    //        BASE CONFIG
    // *********************************
    public static String RAW_JSON_CONFIG = null;
    // *********************************
    //        DATABASE CONFIG
    // *********************************
    public static String DB_HOST = "localhost";
    public static String DB_DATABASE = "discord_bot";
    public static String DB_USERNAME = "root";
    public static String DB_PASSWORD = "";
    public static int DB_PORT = 3306;
    // *********************************
    //        BOT CONFIG
    // *********************************
    public static String BOT_TOKEN = "TOKEN";
    // *********************************
    //        TAG CONFIG
    // *********************************
    public static HashMap<String, String> DEFAULT_TAGS = new HashMap<>();
    // *********************************
    //        GUILD CONFIG
    // *********************************
    public static String GUILD_ID = "1234567890";
    public static String GUILD_CONTROL = "0987654321";
    // *********************************
    //        MUSIC CONFIG
    // *********************************
    public static boolean MUSIC_ENABLED = false;
    public static HashMap<String, IStation> STATIONS = new HashMap<>();
    public static IStation CURRENT_STATION = null;

    public static void addStation(IStation station) {
            if (STATIONS.containsKey(station.getName()))
                throw new IllegalArgumentException("Station already exists! " + station.getName());
        STATIONS.put(station.getName(), station);
    }

    protected static void addTag(String tag, String data){
        if (DEFAULT_TAGS.containsKey(tag))
            throw new IllegalArgumentException("Tag already exists!");
        DEFAULT_TAGS.put(tag, data);
    }
}
