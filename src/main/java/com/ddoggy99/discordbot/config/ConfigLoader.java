/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.config;

import com.ddoggy99.discordbot.music.*;
import com.ddoggy99.discordbot.music.playlist.BasicPlaylist;
import com.ddoggy99.discordbot.music.station.BasicStation;
import com.ddoggy99.discordbot.music.station.YouTubeStation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Brock on 9/06/2016.
 */
public class ConfigLoader {

    private static final Logger log = LogManager.getLogger(ConfigLoader.class);

    private static String configFile = "config.json";

    public static void load() {
        try {
            StringBuilder builder = new StringBuilder();
            FileReader reader = new FileReader(new File(configFile));
            int read;
            while ((read = reader.read()) != -1) {
                builder.append((char) read);
            }
            //System.out.println("JSON: " + builder.toString());
            JSONObject json = new JSONObject(builder.toString());
            Config.RAW_JSON_CONFIG = json.toString();

            // *********************************
            // Check required root configs
            // *********************************

            ConfigLoader.checkRequredConfig(json, "bot_token");
            ConfigLoader.checkRequredConfig(json, "database");
            ConfigLoader.checkRequredConfig(json, "guild");
            ConfigLoader.checkRequredConfig(json, "tags");
            ConfigLoader.checkRequredConfig(json, "musicLocations");
            ConfigLoader.checkRequredConfig(json, "stations");

            // *********************************
            // Check required bot configs
            // *********************************

            if (json.getString("bot_token").length() > 0)
                Config.BOT_TOKEN = json.getString("bot_token");
            else
                ConfigLoader.invalidRequiredConfig("bot_token");

            // *********************************
            // Check required database configs
            // *********************************

            log.info("Loading database configuration!");
            JSONObject databaseJSON = json.getJSONObject("database");
            Config.DB_HOST = databaseJSON.getString("host");
            Config.DB_PORT = databaseJSON.getInt("port");
            Config.DB_USERNAME = databaseJSON.getString("username");
            Config.DB_PASSWORD = databaseJSON.getString("password");
            Config.DB_DATABASE = databaseJSON.getString("database");

            // *********************************
            // Check required guild configs
            // *********************************

            JSONObject guildJSON = json.getJSONObject("guild");
            // Required: Guild ID
            ConfigLoader.checkRequredConfig(guildJSON, "guild_id");
            ConfigLoader.checkRequredConfig(guildJSON, "control_channel");

            Config.GUILD_ID = guildJSON.getString("guild_id");
            // TODO: Have option to only listen to this channel
            Config.GUILD_CONTROL = guildJSON.getString("control_channel");

            // *********************************
            // Check tags config
            // *********************************

            JSONArray tagsJSON = json.getJSONArray("tags");
            for (int i = 0; i < tagsJSON.length(); i++)
                Config.addTag(tagsJSON.getJSONObject(i).getString("tag"), tagsJSON.getJSONObject(i).getString("data"));

            // *********************************
            // Check required music locations config
            // *********************************

            JSONArray musicLocJSON = json.getJSONArray("musicLocations");
            boolean musicDisabled = false;
            if (musicLocJSON.length() == 0) {
                log.error("No music locations specified! Disabling music support!");
                musicDisabled = true;
            } else {
                for (int i = 0; i < musicLocJSON.length(); i++) {
                    File dir = new File(musicLocJSON.getString(i));
                    if (!dir.exists()) {
                        log.fatal("Path does not exist! " + dir.getAbsolutePath());
                        // TODO: Use SysExitRef
                        System.exit(-1);
                    }
                    if (!dir.isDirectory()) {
                        log.fatal("Given path is not a directory! " + dir.getAbsolutePath());
                        // TODO: Use SysExitRef
                        System.exit(-1);
                    }
                    if (!dir.canRead()) {
                        log.fatal("Cannot read given directory! " + dir.getAbsolutePath());
                        // TODO: Use SysExitRef
                        System.exit(-1);
                    }
                    MusicDiscoverer.addPossibleLocation(dir);
                }
            }

            // *********************************
            // Check required station configs
            // *********************************

            JSONArray array = json.getJSONArray("stations");
            if (array.length() < 1) {
                log.error("No stations defined! Disabling music support!");
                Config.MUSIC_ENABLED = false;
                musicDisabled = true;
            }
            if (!musicDisabled) {
                Config.MUSIC_ENABLED = true;
                for (int i = 0; i < array.length(); i++) {
                    JSONObject obj = array.getJSONObject(i);

                    ConfigLoader.checkRequredConfig(obj, "name");
                    ConfigLoader.checkRequredConfig(obj, "type");
                    ConfigLoader.checkRequredConfig(obj, "partial");
                    ConfigLoader.checkRequredConfig(obj, "shuffle");
                    ConfigLoader.checkRequredConfig(obj, "repeat");
                    String type = obj.getString("type");
                    switch (type) {
                        case "file":
                            ConfigLoader.checkRequredConfig(obj, "artists");
                            ConfigLoader.checkRequredConfig(obj, "albums");
                            ConfigLoader.checkRequredConfig(obj, "songs");
                            break;
                        case "yt":
                        case "youtube":
                        case "yt-playlist":
                            ConfigLoader.checkRequredConfig(obj, "ids");
                            break;
                        default:
                            log.error("Skipping station type: " + type);
                            continue;
                    }

                    String name = obj.getString("name");
                    boolean partial = obj.getBoolean("partial");
                    boolean shuffle = obj.getBoolean("shuffle");
                    boolean repeat = obj.getBoolean("repeat");

                    if (name == null || name.length() == 0)
                        name = Integer.toString(new Random().nextInt());

                    if (type.equalsIgnoreCase("file")) {
                        JSONArray artists = obj.getJSONArray("artists");
                        JSONArray albums = obj.getJSONArray("albums");
                        JSONArray songs = obj.getJSONArray("songs");

                        MusicDiscoverer discoverer = new MusicDiscoverer();
                        discoverer.setPartialMatchAllowed(partial);
                        discoverer.setAllowedArtists(ConfigLoader.jsonArrayToList(artists));
                        discoverer.setAllowedAlbums(ConfigLoader.jsonArrayToList(albums));
                        discoverer.setAllowedSongs(ConfigLoader.jsonArrayToList(songs));
                        ArrayList<AudioSource> songsList = discoverer.discover();

                        log.info("Processing new station: " + name);
                        BasicPlaylist playlist = new BasicPlaylist();
                        playlist.shuffle(shuffle);
                        playlist.repeat(repeat);
                        for (AudioSource audioSource : songsList)
                            playlist.addSong(audioSource);
                        Config.addStation(new BasicStation(name, playlist));
                    } else if (type.equalsIgnoreCase("yt") || type.equalsIgnoreCase("youtube") || type.equalsIgnoreCase("yt-playlist")){
                        BasicPlaylist playlist = new BasicPlaylist();
                        playlist.shuffle(shuffle);
                        playlist.repeat(repeat);
                        JSONArray ids = obj.getJSONArray("ids");
                        for (int j = 0; j < ids.length(); j++){
                            String id = ids.getString(j);
                            playlist.addSong(AudioSource.createSource(id));
                        }
                        Config.addStation(new YouTubeStation(name, playlist));
                    }
                }
            }
        } catch (IOException e) {
            log.fatal("No config file found!");
            System.exit(-1);
        }
    }

    public static void setConfigFile(String name) {
        configFile = name;
    }

    public static ArrayList<String> jsonArrayToList(JSONArray array) {
        ArrayList<String> list = new ArrayList<>();

        for (int i = 0; i < array.length(); i++)
            list.add(array.getString(i));

        return list;
    }

    public static void checkRequredConfig(JSONObject json, String key) {
        if (!json.has(key))
            ConfigLoader.missingRequiredConfig(key);
    }

    public static void invalidRequiredConfig(String key) {
        log.fatal("Invalid required config key: " + key);
        // TODO: Use SysExitRef
        System.exit(-1);
    }

    public static void missingRequiredConfig(String key) {
        log.fatal("Missing required config key: " + key);
        // TODO: Use SysExitRef
        System.exit(-1);
    }
}
