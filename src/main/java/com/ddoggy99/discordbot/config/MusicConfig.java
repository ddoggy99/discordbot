/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.config;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Brock on 1/07/2016.
 */
public class MusicConfig {

    // *********************************
    //        LAUNCH ARGUMENTS
    // *********************************
    public static final List<String> FFMPEG_STREAM_ARGS = Arrays.asList("./ffmpeg", "-f", "s16be", "-ac", "2", "-ar", "48000", "-map", "a", "-", "-i");
    public static final List<String> YOUTUBEDL_STREAM_ARGS = Arrays.asList("python", "./youtube-dl", "-q", "-f", "bestaudio/best", "--no-playlist", "-o", "-", "--");
    public static final List<String> FFMPEG_YTDL_STREAM_ARGS = Arrays.asList("./ffmpeg", "-f", "s16be", "-ac", "2", "-ar", "48000", "-map", "a", "-", "-i", "-");
    public static final List<String> YOUTUBEDL_PLAYLIST_INFO = Arrays.asList("python", "./youtube-dl", "-j", "-q");
}
