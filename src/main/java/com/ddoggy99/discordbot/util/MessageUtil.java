/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.util;

import com.ddoggy99.discordbot.DiscordBot;
import net.dv8tion.jda.entities.Channel;
import net.dv8tion.jda.entities.TextChannel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by Brock on 18/06/2016.
 */
// TODO: Move to Util module
public class MessageUtil {

    private static Logger log = LogManager.getLogger(MessageUtil.class);

    public static boolean sendMessageAsync(Channel channel, String message) {
        return sendMessageAsync(channel.getId(), message);
    }

    /**
     * Sends a message to a given channel
     * @param channelID ID of TextChannel
     * @param message Message to send
     * @return True if successful or false if not
     */
    public static boolean sendMessageAsync(String channelID, String message) {
        TextChannel channel = DiscordBot.getJDA().getTextChannelById(channelID);
        if (channel != null){
            channel.sendMessageAsync(message, null);
            return true;
        }
        log.fatal("Unknown channel!");
        return false;
    }
}
