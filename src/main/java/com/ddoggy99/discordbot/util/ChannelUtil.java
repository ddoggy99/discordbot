/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.util;

import com.ddoggy99.discordbot.DiscordBot;
import net.dv8tion.jda.entities.Guild;
import net.dv8tion.jda.entities.TextChannel;
import net.dv8tion.jda.entities.VoiceChannel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * Created by Brock on 11/07/2016.
 */
public class ChannelUtil {

    private static Logger log = LogManager.getLogger(ChannelUtil.class);

    public static VoiceChannel getVoiceChannelByName(String channelName) {
        return getVoiceChannelByName(channelName, false);
    }

    public static VoiceChannel getVoiceChannelByName(String channelName, boolean partial) {
        List<VoiceChannel> channels = DiscordBot.getJDA().getVoiceChannels();
        channelName = channelName.toLowerCase();
        log.trace("Searching for {} (partial:{}) in {} matches", channelName, partial, channels.size());
        for (VoiceChannel channel : channels) {
            log.trace("Channel: " + channel.getName());
            if (partial) {
                if (channel.getName().toLowerCase().contains(channelName)) {
                    return channel;
                }
            } else {
                if (channel.getName().equalsIgnoreCase(channelName)) {
                    return channel;
                }
            }
        }
        return null;
    }

    public static Guild getGuildOfVChannel(String channelID){
        List<VoiceChannel> channels = DiscordBot.getJDA().getVoiceChannels();
        for (VoiceChannel channel : channels)
            if (channel.getId().equalsIgnoreCase(channelID))
                return channel.getGuild();
        return null;
    }

    public static Guild getGuildOfTChannel(String channelID){
        List<TextChannel> channels = DiscordBot.getJDA().getTextChannels();
        for (TextChannel channel : channels)
            if (channel.getId().equalsIgnoreCase(channelID))
                return channel.getGuild();
        return null;
    }
}
