/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.util;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Brock on 30/06/2016.
 */
public class FileUtil {

    /**
     * Returns a list of files inside a directory, recursively
     *
     * @param folder Folder to be searched
     * @return ArrayList of file paths
     */
    public static ArrayList<String> searchFolder(String folder) {
        return FileUtil.searchFolder(new File(folder));
    }

    /**
     * Returns a list of files inside a directory, recursively
     *
     * @param folder Folder to be searched
     * @return ArrayList of file paths
     */
    public static ArrayList<String> searchFolder(File folder) {
        File[] files = folder.listFiles();
        ArrayList<String> list = new ArrayList<>();
        if (files == null)
            throw new RuntimeException("Unable to read folder " + folder.getAbsolutePath());
        for (File sub : files) {
            if (sub.isDirectory()) {
                list.addAll(searchFolder(sub));
            } else {
                list.add(sub.getAbsolutePath());
            }
        }
        return list;
    }
}
