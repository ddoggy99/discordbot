/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.util;

import net.dv8tion.jda.events.message.MessageReceivedEvent;

/**
 * Created by Brock on 10/06/2016.
 */
// TODO: Move to Util module
public class CommandUtil {

    public static boolean isMessageCommand(MessageReceivedEvent event) {
        if (event.getMessage().getContent().startsWith("!")) {
            return true;
        }
        return false;
    }
}
