/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.util;

import com.ddoggy99.discordbot.DiscordBot;
import net.dv8tion.jda.entities.User;
import net.dv8tion.jda.entities.VoiceChannel;

/**
 * Created by Brock on 11/07/2016.
 */
public class UserUtil {

    public static VoiceChannel getConnectedVoiceChannel(String userID){
        for (VoiceChannel channel : DiscordBot.getJDA().getVoiceChannels())
            for (User user : channel.getUsers())
                if (user.getId().equalsIgnoreCase(userID)) return channel;
        return null;
    }
}
