/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.handlers;

import com.ddoggy99.discordbot.DiscordBot;
import com.ddoggy99.discordbot.events.CommandNotImplementedEvent;
import com.ddoggy99.discordbot.events.CustomEvent;
import com.ddoggy99.discordbot.events.jda.MusicPlayEvent;
import com.ddoggy99.discordbot.util.MessageUtil;
import net.dv8tion.jda.events.Event;
import net.dv8tion.jda.hooks.ListenerAdapter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by Brock on 4/06/2016.
 */
public class CustomEventHandler extends ListenerAdapter {

    private Logger log = LogManager.getLogger(this);

    /*public void onCommandFailedEvent(CommandFailedEvent event) {
        System.out.println("Processing CommandFailedEvent! " + event.getReason());
        event.getMessage().getChannel().sendMessageAsync(event.getReason(), null);
    }*/

    public void onCommandNotImplementedEvent(CommandNotImplementedEvent event) {
        System.out.println("Processing CommandFailedEvent! " + event.getMessage().getContent());
        event.getMessage().getChannel().sendMessageAsync("Command or subcommand not yet implemented! " + event.getMessage().getContent(), null);
    }

    public void onMusicPlayEvent(MusicPlayEvent event){
        MessageUtil.sendMessageAsync(DiscordBot.getControlChannelID(), event.getMessage());
        DiscordBot.getJDA().getAccountManager().setGame(event.getAudioSource().getSong() + " by " + event.getAudioSource().getArtist());
    }

    public void onCustomEvent(CustomEvent event) {
        log.debug("Handling event " + event.getClass().getName());
    }

    @Override
    public void onEvent(Event event) {
        //if (event instanceof CommandFailedEvent) onCommandFailedEvent((CommandFailedEvent) event);
        //else
        if (event instanceof CommandNotImplementedEvent) onCommandNotImplementedEvent((CommandNotImplementedEvent) event);
        else if (event instanceof MusicPlayEvent) onMusicPlayEvent((MusicPlayEvent) event);

        else if (event instanceof CustomEvent) onCustomEvent((CustomEvent) event);
    }
}
