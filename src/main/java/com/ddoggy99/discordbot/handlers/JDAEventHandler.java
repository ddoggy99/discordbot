/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.handlers;

import com.ddoggy99.discordbot.DiscordBot;
import com.ddoggy99.discordbot.config.Config;
import com.ddoggy99.discordbot.util.MessageUtil;
import net.dv8tion.jda.entities.TextChannel;
import net.dv8tion.jda.events.DisconnectEvent;
import net.dv8tion.jda.events.ReadyEvent;
import net.dv8tion.jda.events.ReconnectedEvent;
import net.dv8tion.jda.hooks.ListenerAdapter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * Created by Brock on 4/06/2016.
 */
public class JDAEventHandler extends ListenerAdapter {

    private final Logger log = LogManager.getLogger(this);

    private boolean PAUSE_ON_DISCONNECT = true;
    private boolean PAUSED_ON_DISCONNECT = false;

    @Override
    public void onReady(ReadyEvent event) {
        TextChannel channel = event.getJDA().getTextChannelById(Config.GUILD_CONTROL);
        channel.sendMessageAsync("Ready to serve " + event.getJDA().getGuildById(Config.GUILD_ID).getName(), null);
    }

    @Override
    public void onDisconnect(DisconnectEvent event) {
        boolean serverDisconnected = event.isClosedByServer();
        if (serverDisconnected) {
            log.fatal("Was disconnected by server!");
        } else {
            log.error("Lost connection to server");
            if (this.PAUSE_ON_DISCONNECT && Config.CURRENT_STATION != null && Config.CURRENT_STATION.isPlaying()) {
                this.PAUSED_ON_DISCONNECT = true;
                Config.CURRENT_STATION.pause();
            }
        }
    }

    @Override
    public void onReconnect(ReconnectedEvent event) {
        log.info("Reconnected to server!");
        if (this.PAUSED_ON_DISCONNECT) {
            this.PAUSED_ON_DISCONNECT = false;
            Config.CURRENT_STATION.start();
        }
    }
}
