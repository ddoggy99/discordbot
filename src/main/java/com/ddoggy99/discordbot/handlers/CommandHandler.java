/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.handlers;

import com.ddoggy99.discordbot.DiscordBot;
import com.ddoggy99.discordbot.commands.ICommand;
import com.ddoggy99.discordbot.events.CommandUnknownEvent;
import com.ddoggy99.discordbot.util.CommandUtil;
import net.dv8tion.jda.entities.Message;
import net.dv8tion.jda.events.message.MessageReceivedEvent;
import net.dv8tion.jda.hooks.ListenerAdapter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

/**
 * Created by Brock on 29/06/2016.
 */
public class CommandHandler extends ListenerAdapter {

    private static Logger log = LogManager.getLogger(CommandHandler.class);

    private static ArrayList<ICommand> commandList = new ArrayList<>();

    public static void registerCommand(ICommand command) {
        for (ICommand registeredCommand : commandList) {
            if (command.getCommand().equalsIgnoreCase(registeredCommand.getCommand())){
                log.error("Command is already registered! Adding with conflict with the currently registered commands! Command: " + registeredCommand.getCommand());
            }
            if (command.getAliases() != null && !command.getAliases().isEmpty() && registeredCommand.getAliases() != null && !registeredCommand.getAliases().isEmpty()){
                for (String alias : command.getAliases()){
                    if (registeredCommand.getAliases().contains(alias)){
                        log.error("Alias is already registered! This will conflict with the currently registered commands!");
                        log.error("Registered Command: " + registeredCommand.getCommand() + ", Alias: " + alias);
                    }
                }
            }
        }
        log.info("Registering new command: " + command.getCommand());
        commandList.add(command);
    }

    public static ICommand getCommand(String name) {
        for (ICommand command : getCommandList()) {
            if (command.getCommand().equalsIgnoreCase(name) || (command.getAliases() != null && command.getAliases().contains(name)))
                return command;
        }
        log.debug("Unknown command: " + name);
        return null;
    }

    public static ArrayList<ICommand> getCommandList(){
        return CommandHandler.commandList;
    }

    public void onMessageReceived(MessageReceivedEvent event) {
        if (!CommandUtil.isMessageCommand(event)) return;
        String message = event.getMessage().getContent();
        String[] split = message.substring(1).split(" ", 2);
        if (split.length != 1)
            processCommand(event.getMessage(), split[0], split[1]);
        else
            processCommand(event.getMessage(), message, null);
    }

    public void processCommand(Message message, String command, String args) {
        command = command.toLowerCase();
        if (command.startsWith("!")) command = command.substring(1);

        ICommand iCommand = CommandHandler.getCommand(command);
        if (iCommand != null) {
            iCommand.onCommand(message, args);
            log.debug("Processing command: " + command + ", [" + args + "]");
        } else {
            DiscordBot.getEventManager().handle(new CommandUnknownEvent(message.getJDA(), 404, command));
        }
    }
}
