/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot;

import com.ddoggy99.discordbot.commands.CommandLeave;
import com.ddoggy99.discordbot.commands.CommandTag;
import com.ddoggy99.discordbot.commands.music.*;
import com.ddoggy99.discordbot.commands.CommandHelp;
import com.ddoggy99.discordbot.commands.CommandJoin;
import com.ddoggy99.discordbot.config.Config;
import com.ddoggy99.discordbot.config.ConfigLoader;
import com.ddoggy99.discordbot.db.MySQL;
import com.ddoggy99.discordbot.handlers.CommandHandler;
import com.ddoggy99.discordbot.handlers.CustomEventHandler;
import com.ddoggy99.discordbot.handlers.JDAEventHandler;
import com.ddoggy99.discordbot.lib.SysExitRef;
import com.ddoggy99.discordbot.music.station.IStation;
import net.dv8tion.jda.JDA;
import net.dv8tion.jda.JDABuilder;
import net.dv8tion.jda.entities.Guild;
import net.dv8tion.jda.entities.TextChannel;
import net.dv8tion.jda.entities.impl.JDAImpl;
import net.dv8tion.jda.hooks.IEventManager;
import net.dv8tion.jda.managers.AudioManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.LoggerConfig;

import javax.security.auth.login.LoginException;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Main class
 * <p>
 * Created by Brock on 4/06/2016.
 */
public class DiscordBot {

    private static final Logger log = LogManager.getLogger(DiscordBot.class);

    private static JDA jda;
    private static boolean DEBUG_MODE = false;

    public static void main(String[] args) {
        log.info("Starting DiscordBot");
        processArgs(args);
        if (DEBUG_MODE) { // DEBUG
            LoggerContext context = (LoggerContext) LogManager.getContext(false);
            LoggerConfig config = context.getConfiguration().getLoggerConfig(DiscordBot.class.getName());
            config.setLevel(Level.ALL);
            context.updateLoggers();
            log.debug("Enabling DEBUG mode...");
        }

        // Load config
        ConfigLoader.load();
        // Set MySQL Connection Info
        MySQL.setConnection(Config.DB_HOST, Config.DB_PORT, Config.DB_USERNAME, Config.DB_PASSWORD, Config.DB_DATABASE);
        // Register Commands
        CommandHandler.registerCommand(new CommandHelp());
        CommandHandler.registerCommand(new CommandTag());
        CommandHandler.registerCommand(new CommandJoin());
        CommandHandler.registerCommand(new CommandLeave());
        CommandHandler.registerCommand(new CommandPlay());
        CommandHandler.registerCommand(new CommandPause());
        CommandHandler.registerCommand(new CommandStop());
        CommandHandler.registerCommand(new CommandStation());
        CommandHandler.registerCommand(new CommandStations());
        CommandHandler.registerCommand(new CommandAdd());
        CommandHandler.registerCommand(new CommandAddPlaylist());
        CommandHandler.registerCommand(new CommandVolume());

        // List stations
        for (IStation station : Config.STATIONS.values()) {
            log.info("Station found: " + station.getName());
            log.info("Contains " + station.getPlaylist().size());
            Thread thread = new Thread(station);
            thread.setName("Station-" + station.getName());
            thread.start();
        }
        //System.exit(-1);
        // Build JDA
        JDABuilder builder = new JDABuilder();
        builder.setBotToken(Config.BOT_TOKEN);
        builder.addListener(new JDAEventHandler());
        builder.addListener(new CommandHandler());
        builder.addListener(new CustomEventHandler());
        try {
            jda = builder.buildBlocking();
            log.info("Loading complete!");
        } catch (LoginException e) {
            log.fatal("Unable to login!");
            e.printStackTrace();
            // TODO: Use SysExitRef
            System.exit(-1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        boolean shutdown = false;
        Scanner scanner = new Scanner(System.in);
        while (!shutdown){
            String line = scanner.nextLine();
            if (line.equalsIgnoreCase("shutdown")){
                log.info("Received shutdown command from console!");
                shutdown = true;
            }
        }
        jda.shutdown();
        System.exit(0);
    }

    private static void processArgs(String[] args) {
        if (args != null && args.length != 0) {
            List<String> list = Arrays.asList(args);
            for (int i = 0; i < list.size(); i++) {
                String arg = list.get(i).substring(1);
                switch (arg) {
                    case "debug":
                        DEBUG_MODE = true;
                        break;
                    case "config":
                        i++;
                        String file = list.get(i);
                        log.info("Using config file: " + file);
                        ConfigLoader.setConfigFile(checkConfigFile(file));
                        break;
                    default:
                        log.warn("Unknown argument: " + arg);
                }
            }
        } else {
            log.info("No arguments passed");
        }
    }

    private static String checkConfigFile(String config) {
        File file = new File(config);
        if (!file.exists()) {
            log.fatal("Config file not found!");
            System.exit(SysExitRef.CONFIG_NOT_FOUND);
        } else if (!file.isFile()) {
            log.fatal("Config specified is not a file!");
            System.exit(SysExitRef.CONFIG_NOT_FOUND);
        } else if (!file.canRead()) {
            log.fatal("Unable to read config file!");
            System.exit(SysExitRef.CONFIG_NOT_FOUND);
        }
        return config;
    }

    public static String getGuildID() {
        return Config.GUILD_ID;
    }

    public static Guild getGuild() {
        return getGuild(getGuildID());
    }

    public static Guild getGuild(String guildid) {
        return getJDA().getGuildById(guildid);
    }

    public static String getControlChannelID() {
        return Config.GUILD_CONTROL;
    }

    public static TextChannel getControlChannel() {
        return getTextChannel(getControlChannelID());
    }

    public static TextChannel getTextChannel(String channelid) {
        return getJDA().getTextChannelById(channelid);
    }

    public static JDAImpl getJDAImpl() {
        return (JDAImpl) getJDA();
    }

    public static JDA getJDA() {
        if (jda == null) {
            log.fatal("JDA is not yet initialised");
        }
        return jda;
    }

    public static IEventManager getEventManager() {
        return getJDAImpl().getEventManager();
    }

    public static AudioManager getAudioManager() {
        return getAudioManager(getGuild());
    }

    public static AudioManager getAudioManager(Guild guild) {
        return getGuild(guild.getId()).getAudioManager();
    }
}
