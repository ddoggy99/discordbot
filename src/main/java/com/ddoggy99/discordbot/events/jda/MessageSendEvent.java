/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.events.jda;

import com.ddoggy99.discordbot.events.CustomEvent;

/**
 * Created by Brock on 2/07/2016.
 */
public class MessageSendEvent extends CustomEvent {

    private String channel;
    private String message;

    public MessageSendEvent(String channel, String message) {
        super(null, 200);
        this.channel = channel;
        this.message = message;
    }

    public String getChannel(){
        return this.channel;
    }

    public String getMessage(){
        return this.message;
    }
}
