/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.events;

import net.dv8tion.jda.JDA;
import net.dv8tion.jda.entities.Message;

/**
 * Created by Brock on 4/06/2016.
 */
public class CommandNotImplementedEvent extends CustomEvent {

    private Message message;

    public CommandNotImplementedEvent(JDA api, Message message) {
        super(api, 200);
        this.message = message;
    }

    public Message getMessage() {
        return this.message;
    }
}
