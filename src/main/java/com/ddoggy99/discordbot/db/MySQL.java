/*
 *     Copyright (C) 2016 ddoggy99
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ddoggy99.discordbot.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * MySQL Wrapper class
 * <p>
 * Created by Brock on 8/06/2016.
 */
public class MySQL {

    private static String HOST = "localhost";
    private static int PORT = 3306;
    private static String USERNAME = "root";
    private static String PASSWORD = "";
    private static String DATABASE = "discord_bot";

    private static String URL;

    public static void executeSQL(String sql) {
        try (Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD)) {
            Statement statement = connection.createStatement();
            statement.execute(sql);
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<HashMap> executeQuery(String sql) {
        try (Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD)) {
            Statement statement = connection.createStatement();
            ResultSet results = statement.executeQuery(sql);
            int columns = results.getMetaData().getColumnCount();
            ArrayList<HashMap> list = new ArrayList<>();
            HashMap<String, Object> map = new HashMap<>();
            while (results.next()) {
                map.clear();
                for (int i = 1; i <= columns; i++) {
                    map.put(results.getMetaData().getColumnName(i), results.getString(i));
                }
                list.add((HashMap) map.clone());
            }
            statement.close();
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Sets the connection variables, Can pass null or 0 to keep previousily assigned variables
     *
     * @param host     Host of the Database Server
     * @param port     Port of the Database Server
     * @param username Username to login with
     * @param password Password to login with
     * @param database Database to use
     */
    public static void setConnection(String host, int port, String username, String password, String database) {
        if (host != null) HOST = host;
        if (port <= 0 || port >= 65535) PORT = port;
        if (username != null) USERNAME = username;
        if (password != null) PASSWORD = password;
        if (database != null) DATABASE = database;
        URL = "jdbc:mysql://" + HOST + ":" + PORT + "/" + DATABASE + "?useLegacyDatetimeCode=false&serverTimezone=Australia/Sydney";
    }

    public static String buildInsertQuery(String database, Object... args) {
        return buildInsertQueryIgnore(database, false, args);
    }

    public static String buildInsertQueryIgnore(String database, boolean ignore, Object... args) {
        StringBuilder builder = new StringBuilder();
        builder.append("INSERT");
        if (ignore) builder.append(" IGNORE");
        builder.append(" INTO ");
        builder.append(database);
        builder.append(" VALUES (");
        for (Object arg : args)
            if (arg != null) builder.append("\"" + escapeIfString(arg) + "\",");
            else builder.append("null, ");
        if (builder.toString().endsWith(", ")) builder.deleteCharAt(builder.length() - 2); // Remove extra ", " at the end of the build
        if (builder.toString().endsWith(",")) builder.deleteCharAt(builder.length() - 1); //  Remove extra "," at the end of the build
        builder.append(");");
        return builder.toString();
    }

    public static Object escapeIfString(Object obj){
        String temp = null;
        if (obj instanceof String) {
            temp = (String) obj;
            temp = temp.replaceAll("\"", "\\\\\"");
        }
        return temp != null ? temp : obj;
    }

}
